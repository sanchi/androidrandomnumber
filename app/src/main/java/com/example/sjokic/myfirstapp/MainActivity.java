package com.example.sjokic.myfirstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView maxText = (TextView)findViewById(R.id.editText);
        maxText.setText("6");
    }

    public void generateRandom(View views){
        //Intent intent = new Intent(this, SecondActivity.class);
        TextView textView = (TextView)findViewById(R.id.editText);
        String maxText = textView.getText().toString();
        if(maxText.isEmpty()){
            maxText = "6";
        }
        Integer max = Integer.valueOf(maxText);
        Random random = new Random();
        Integer next = random.nextInt(max)+1;

        Toast.makeText(this, String.valueOf(next), Toast.LENGTH_SHORT).show();

        //intent.putExtra("text_to_show", String.valueOf(Math.round(Math.ceil(Math.random() * max))));
        //startActivity(intent);
    }
}

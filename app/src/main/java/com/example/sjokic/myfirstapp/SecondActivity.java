package com.example.sjokic.myfirstapp;

import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        String string = getIntent().getStringExtra("text_to_show");
        Toast.makeText(this, string, Toast.LENGTH_SHORT).show();

        Intent restartMainIntent = new Intent(this, MainActivity.class);
        startActivity(restartMainIntent);
    }
}
